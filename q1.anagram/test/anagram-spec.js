describe('should check the anagram properly', () => {
    it('skipping both word parameter should throw error', () => {
        expect( function(){ isAnagram(); } ).toThrow(new Error("Please provide two valid word to check anagram"));
    })
    it('skip first word should throw error', () => {
        expect( function(){ isAnagram('', 'eat'); } ).toThrow(new Error("Please provide two valid word to check anagram"));
    })
    it('skip secound word should throw error', () => {
        expect( function(){ isAnagram('madam'); } ).toThrow(new Error("Please provide two valid word to check anagram"));
    })
    it('unmatch world length should be return false', () => {
        expect(isAnagram('madam', 'sir')).toBe(false);
    })
    it("'bleat', 'table' should be anagram", () => {
        expect(isAnagram('bleat', 'table')).toBe(true);
    })
    it("'eat', 'tar' should  not be anagram", () => {
        expect(isAnagram('bleat', 'table')).toBe(true);
    })
});

