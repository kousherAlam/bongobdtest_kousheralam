/**
 * With the given vehicle interface we can create object in diffrecent pattern, 
 * They are call creational pattern, there are many creational pattern, 
 * (1) Class Pattern
 * (2) Singleton 
 * (3) Factory 
 */
export interface Vehicle {
    set_num_of_wheels: (wheels: number) => number;
    set_num_of_passengers: (passenger: number) => number;
    has_gas: () => boolean;
}
class BaseVehicle implements Vehicle {
    constructor(
        private wheels: number,
        private passenget: number,
        private hasGas: boolean,
    ) { }
    set_num_of_wheels(wheels: number) {
        this.wheels = wheels;
        return this.wheels;
    }

    set_num_of_passengers(passenger: number) {
        this.passenget = passenger;
        return this.passenget;
    };

    has_gas() {
        return this.hasGas;
    }
}


/**
 * Class Creational Pattern 
 */
{
    class Car extends BaseVehicle {
        private _model: string;
        get model() { return this._model; }
        set model(value: string) { this._model = value; }
    }

    class Plane extends BaseVehicle {
        private _isFlying: string;
        get isFlying() { return this._isFlying; }
        set isFlying(value: string) { this._isFlying = value; }
    }

    // creating object using class patter

    const car = new Car(4, 1, true);
    const plane = new Plane(2, 20, true);
}


/**
 * singleton pattern
 * it allow to prevent creating instance more than one 
 */
{
    class Car {
        private static instance: Car;
        private _wheels: number;
        private _passenger: number;
        private _hasGas: boolean;
        constructor() { }

        public static getInstance(): Car {
            if (!Car.instance) {
                Car.instance = new Car();
            }
            return Car.instance;
        }

        public get wheel() { return this._wheels; }
        public set wheel(value: number) { this._wheels = value; }

        public get passenger() { return this._passenger; }
        public set passenger(value: number) { this._passenger = value; }

        public get hasGas() { return this._hasGas; }
        public set hasGas(value: boolean) { this._hasGas = value; }
    }

    const car = Car.getInstance();
}



/**
 * Factory pattern
 */
{
    class Car extends BaseVehicle {
        private _model: string;
        get model() { return this._model; }
        set model(value: string) { this._model = value; }
    }

    class Plane extends BaseVehicle {
        private _isFlying: string;
        get isFlying() { return this._isFlying; }
        set isFlying(value: string) { this._isFlying = value; }
    }

    class CarFactory {
        crate4PassengerCar(): Car {
            return new Car(4, 4, true);
        }

        create2PassengerCar(): Car {
            return new Car(4, 2, true);
        }
    }
    class PlaneFactory {
        crate10PassengerPlane(): Plane {
            return new Plane(2, 10, true);
        }

        create20PassengerPlane(): Plane {
            return new Plane(2, 20, true);
        }
    }

    const carFactory = new CarFactory();
    const fourPassengerCar = carFactory.crate4PassengerCar();

    const planeFactory = new PlaneFactory();
    const tenPassengPlane = planeFactory.crate10PassengerPlane();
}