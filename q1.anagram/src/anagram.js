/**
 * isAnagram function will check the two word and return a boolean value 
 * depend on the is two word is anagram or not. the rules for the anagram is 
 * (1) the two word must be in smae lenght 
 * (2) count of charecter in world should be same
 * @param word1 string  
 * @param word2 string  
 */
function isAnagram(word1, word2) {
    if (!word1 || !word2) {
        throw new Error("Please provide two valid word to check anagram");
    }
    
    if (word1.length !== word2.length) {
        return false;
    }

    // to track double or more charecter in the word, we will put the index here
    // if we find the charecter and for secound or more occrence will will check the index 
    // is already here or not, if here then we will skip it.  
    var findsIndex = {};
   
    // loop throught the first world
    for(var i = 0; i<word1.length; i++){
        var isFindIt = false;

        // loop throught the secound word to find the charecter is in the secound word. 
        for(var j=0; j<word2.length; j++){
            if(word1[i] === word2[j]){
                if(findsIndex[j]){ // check we match it before or not. if it is then skip it.
                    continue;
                } else {
                    findsIndex[j] = 1; // add the charecter in the finding object.
                    isFindIt = true;
                }
            }
        }
        if(!isFindIt){
            return false;
        }
    }

    return true;
}
