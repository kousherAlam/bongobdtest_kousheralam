var CONFIGURATION = {
    REFs: {
        PLAYER: null,
        BTN_BACKWARD: null,
        BTN_FORWARD: null,
        BTN_PLAY_PAUSE: null,
        CURRENT_TIME: null,
        TOTAL_TIME: null,
        TIME_SLIDER: null,
        BTN_FULLSCREEN: null,
        BTN_CAPTIONS: null,
        PLAYER_HOLDER: null,
        CAPTION_TEXT: null,
    },
    CONTROL_VISIBILITY_TIMER: null, 
    CONTROL_AUTOHIDE_TIME: 2000, 
    FORDWARD_TIME: 10,
    BACKWARD_TIME: 10,
    STATE: {
        DURATION: 0,
        CURRENT_TIME: 0,
        IS_PLAYING: false,
        IS_FULLSCREEN: false,
        IS_CAPTION: false,
    },
    CURRENT_VIDEO_CAPTIONS: [
        "1st 3 secound captions",
        "2nd 3 secound captions",
        "3rd 3 secound captions",
        "4th 3 secound captions",
    ],
    KEY_MPAS: {
        PLAY_PAUSE: 32, 
        FULL_SCREEN: 70, 
        CAPTION_: 67, 
        FORWARD: 39, 
        BACKWARD: 37, 
    }
}

function togglePlayPause(event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    if (CONFIGURATION.STATE.IS_PLAYING) {
        CONFIGURATION.REFs.PLAYER.pause();
    } else {
        CONFIGURATION.REFs.PLAYER.play();
    }
}


function forward(event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    var goTo = CONFIGURATION.REFs.PLAYER.currentTime + CONFIGURATION.FORDWARD_TIME;
    CONFIGURATION.REFs.PLAYER.currentTime = goTo;
}

function backward(event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    var goTo = CONFIGURATION.REFs.PLAYER.currentTime - CONFIGURATION.BACKWARD_TIME;
    CONFIGURATION.REFs.PLAYER.currentTime = goTo;
}

function updateCaption() {

}

function millisecondsToStr(temp) {
    const hours = Math.floor((temp %= 86400) / 3600),
        minutes = Math.floor((temp %= 3600) / 60),
        seconds = temp % 60;

    var time = '';

    if (hours < 10) {
        time += '0' + hours + ":";
    } else {
        time += hours + ":";
    }

    if (minutes < 10) {
        time += '0' + minutes + ":";
    } else {
        time += minutes + ":";
    }

    if (seconds < 10) {
        time += '0' + seconds;
    } else {
        time += seconds;
    }
    return time;
}

function updateCurrentTime() {
    var theTime = millisecondsToStr(Math.ceil(CONFIGURATION.STATE.CURRENT_TIME));
    CONFIGURATION.REFs.CURRENT_TIME.innerHTML = theTime;
}

function updateVideoProgressSlider() {
    var theTime = Math.ceil(CONFIGURATION.STATE.CURRENT_TIME / CONFIGURATION.STATE.DURATION * 100);
    theTime = theTime ? theTime : 0;
    CONFIGURATION.REFs.TIME_SLIDER.value = theTime;
}

function showTotalTime() {
    var theTime = millisecondsToStr(Math.ceil(CONFIGURATION.STATE.DURATION));
    CONFIGURATION.REFs.TOTAL_TIME.innerHTML = theTime;
}

function updateUIForFullScreen(){
    CONFIGURATION.REFs.PLAYER_HOLDER.classList.add('full-screen');
    CONFIGURATION.STATE.IS_FULLSCREEN = true;
    CONFIGURATION.CONTROL_VISIBILITY_TIMER =  setTimeout(function(){
        CONFIGURATION.REFs.PLAYER_HOLDER.classList.add('remove-all-control');
    }, CONFIGURATION.CONTROL_AUTOHIDE_TIME);
}

function updateUIForFullScreenExit(){
    CONFIGURATION.REFs.PLAYER_HOLDER.classList.remove('full-screen');
    CONFIGURATION.REFs.PLAYER_HOLDER.classList.remove('remove-all-control');
    CONFIGURATION.STATE.IS_FULLSCREEN = false;
    if(CONFIGURATION.CONTROL_VISIBILITY_TIMER){
        clearTimeout(CONFIGURATION.CONTROL_VISIBILITY_TIMER);
        CONFIGURATION.CONTROL_VISIBILITY_TIMER = null;
    }
}

function updateVideoWhenTimechagnge(event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    var parcent = CONFIGURATION.REFs.TIME_SLIDER.value;
    var videoTime = CONFIGURATION.STATE.DURATION / 100 * parcent;
    videoTime = videoTime ? videoTime : 0;
    CONFIGURATION.REFs.PLAYER.currentTime = videoTime;
}

function exitfullScreenHandeler(event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    if (
        document.webkitIsFullScreen === false ||
        document.mozFullScreen === false ||
        document.msFullscreenElement === false ||
        document.fullscreen === false
    ) {
        updateUIForFullScreenExit();
    }

}


function toggleFullScreen(event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    if (CONFIGURATION.STATE.IS_FULLSCREEN) {
        var isDone = true;
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        } else {
            isDone = false;
        }
        if(isDone){
            updateUIForFullScreenExit();
        }
    } else {
        var isDone = true;
        if (CONFIGURATION.REFs.PLAYER_HOLDER.requestFullscreen) {
            CONFIGURATION.REFs.PLAYER_HOLDER.requestFullscreen();
        } else if (CONFIGURATION.REFs.PLAYER_HOLDER.mozRequestFullScreen) {
            CONFIGURATION.REFs.PLAYER_HOLDER.mozRequestFullScreen();
        } else if (CONFIGURATION.REFs.PLAYER_HOLDER.webkitRequestFullscreen) {
            CONFIGURATION.REFs.PLAYER_HOLDER.webkitRequestFullscreen();
        } else if (CONFIGURATION.REFs.PLAYER_HOLDER.msRequestFullscreen) {
            CONFIGURATION.REFs.PLAYER_HOLDER.msRequestFullscreen();
        } else {
            isDone = false;
        }
        if(isDone){
            updateUIForFullScreen();
        }
    }
}

function toggleCaption(event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    if (CONFIGURATION.STATE.IS_CAPTION) {
        CONFIGURATION.STATE.IS_CAPTION = false;
        CONFIGURATION.REFs.CAPTION_TEXT.style.display = 'none';
    } else {
        CONFIGURATION.STATE.IS_CAPTION = true;
        CONFIGURATION.REFs.CAPTION_TEXT.style.display = 'block';
    }
}

function checkCaption() {
    if (CONFIGURATION.STATE.IS_CAPTION) {
        var sec = Math.ceil(CONFIGURATION.STATE.CURRENT_TIME) - 1;
        sec = Math.floor((sec ? sec : 1) / 3);
        if (CONFIGURATION.CURRENT_VIDEO_CAPTIONS.length > 0 &&
            CONFIGURATION.CURRENT_VIDEO_CAPTIONS.length > sec
        ) {
            CONFIGURATION.REFs.CAPTION_TEXT.innerHTML = CONFIGURATION.CURRENT_VIDEO_CAPTIONS[sec];
        } else {
            CONFIGURATION.REFs.CAPTION_TEXT.innerHTML = 'no caption data in this time.'
        }
    }
}

function showControlFor5SecWhileFullScreen(event){
    event.preventDefault();
    if(CONFIGURATION.STATE.IS_FULLSCREEN){
        CONFIGURATION.REFs.PLAYER_HOLDER.classList.remove('remove-all-control');
        if(CONFIGURATION.CONTROL_VISIBILITY_TIMER){
            clearTimeout(CONFIGURATION.CONTROL_VISIBILITY_TIMER);
        }
        CONFIGURATION.CONTROL_VISIBILITY_TIMER = setTimeout(function(){
            CONFIGURATION.REFs.PLAYER_HOLDER.classList.add('remove-all-control');
        }, CONFIGURATION.CONTROL_AUTOHIDE_TIME);
    }
}

function initPlayerReference() {
    CONFIGURATION.REFs.PLAYER = document.getElementById("the_video_player");
    CONFIGURATION.REFs.BTN_FORWARD = document.getElementById("btn_forward");
    CONFIGURATION.REFs.BTN_BACKWARD = document.getElementById("btn_backward");
    CONFIGURATION.REFs.BTN_PLAY_PAUSE = document.getElementById("btn_play_pause");
    CONFIGURATION.REFs.CURRENT_TIME = document.getElementById("current-time");
    CONFIGURATION.REFs.TOTAL_TIME = document.getElementById("total-time");
    CONFIGURATION.REFs.TIME_SLIDER = document.getElementById("time-slider");
    CONFIGURATION.REFs.BTN_CAPTIONS = document.getElementById("captoins-btn");
    CONFIGURATION.REFs.BTN_FULLSCREEN = document.getElementById("full-screen-btn");
    CONFIGURATION.REFs.PLAYER_HOLDER = document.getElementById("player-holder");
    CONFIGURATION.REFs.CAPTION_TEXT = document.getElementById("captions-text");
}

(function () {
    initPlayerReference();
    showTotalTime();
    updateVideoProgressSlider();
    CONFIGURATION.STATE.DURATION = CONFIGURATION.REFs.PLAYER.duration;
    CONFIGURATION.REFs.PLAYER.addEventListener("play", function () {
        CONFIGURATION.STATE.IS_PLAYING = true;
        CONFIGURATION.REFs.BTN_PLAY_PAUSE.innerHTML = 'Pause';
        CONFIGURATION.STATE.DURATION = CONFIGURATION.REFs.PLAYER.duration;
        showTotalTime();
    });

    CONFIGURATION.REFs.PLAYER.addEventListener("pause", function () {
        CONFIGURATION.STATE.IS_PLAYING = false;
        CONFIGURATION.REFs.BTN_PLAY_PAUSE.innerHTML = 'Play';
    });

    CONFIGURATION.REFs.PLAYER.addEventListener("timeupdate", function () {
        CONFIGURATION.STATE.CURRENT_TIME = CONFIGURATION.REFs.PLAYER.currentTime;
        updateCurrentTime();
        updateVideoProgressSlider();
        checkCaption();
    });

    CONFIGURATION.REFs.BTN_PLAY_PAUSE.addEventListener("click", togglePlayPause);
    CONFIGURATION.REFs.BTN_FORWARD.addEventListener('click', forward);
    CONFIGURATION.REFs.BTN_BACKWARD.addEventListener('click', backward);
    CONFIGURATION.REFs.TIME_SLIDER.addEventListener('change', updateVideoWhenTimechagnge);

    CONFIGURATION.REFs.BTN_FULLSCREEN.addEventListener('click', toggleFullScreen)
    CONFIGURATION.REFs.BTN_CAPTIONS.addEventListener('click', toggleCaption)

    // check for full screen exit 
    document.addEventListener('fullscreenchange', exitfullScreenHandeler);
    document.addEventListener('mozfullscreenchange', exitfullScreenHandeler);
    document.addEventListener('MSFullscreenChange', exitfullScreenHandeler);
    document.addEventListener('webkitfullscreenchange', exitfullScreenHandeler);

    // control the player by the keyboard
    window.addEventListener("keydown", function (event) {
        event.preventDefault();
        event.stopImmediatePropagation();
        switch (event.keyCode) {
            case CONFIGURATION.KEY_MPAS.BACKWARD:
                backward(event);
                break;
            case CONFIGURATION.KEY_MPAS.FORWARD:
                forward(event);
                break;
            case CONFIGURATION.KEY_MPAS.FULL_SCREEN:
                toggleFullScreen(event);
                break;
            case CONFIGURATION.KEY_MPAS.CAPTION_:
                toggleCaption(event);
                break;
            case CONFIGURATION.KEY_MPAS.PLAY_PAUSE:
                togglePlayPause(event);
                break;
        }
        console.log(event.keyCode);
    });

    window.addEventListener('mousemove', showControlFor5SecWhileFullScreen);
    window.addEventListener('click', showControlFor5SecWhileFullScreen);

}());